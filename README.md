# hello-maven-package

Gitlab maven package feature test.

## Usage

Add the following dependency in your `pom.xml` file :

```xml
<dependency>
  <groupId>com.ziggornif</groupId>
  <artifactId>hello-maven-package</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```

And the maven gitlab repository declaration in your `pom.xml` file :

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/29113894/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/29113894/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/29113894/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

See complete example in [examples/use-module](examples/use-module) directory.

## Project setup

```sh
mvn clean install
```

## Run tests

```sh
mvn test
```

<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>