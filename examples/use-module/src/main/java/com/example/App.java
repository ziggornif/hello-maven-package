package com.example;

import com.ziggornif.HelloPackage;

public class App {
  public static void main( String[] args ) {
    System.out.println(HelloPackage.hello(null));
    System.out.println(HelloPackage.hello("Bob"));
  }
}
