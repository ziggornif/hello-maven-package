package com.ziggornif;

import junit.framework.TestCase;

/**
 * Hello package tests
 */
public class HelloPackageTest extends TestCase {

  public void testInstanciation() {
    final HelloPackage helloPackage = new HelloPackage();
    assertNotNull(helloPackage);
  }

  /**
   * Should say hello world
   */
  public void testHello() {
    String resp = HelloPackage.hello(null);
    assertEquals("Hello world !", resp);
  }

  /**
   * Should say hello world if name is empty
   */
  public void testHelloEmpty() {
    String resp = HelloPackage.hello("");
    assertEquals("Hello world !", resp);
  }

  /**
   * Should say hello to somebody
   */
  public void testHelloSomebody() {
    String resp = HelloPackage.hello("Jean-Michel");
    assertEquals("Hello Jean-Michel !", resp);
  }
}
