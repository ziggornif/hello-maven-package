package com.ziggornif;

public class HelloPackage {
  /**
   * Say hello to somebody
   *
   * @param name
   * @return hello string
   */
  public static String hello(String name) {
    if (name != null && !name.isEmpty()) {
      return "Hello " + name + " !";
    }
    return "Hello world !";
  }
}
